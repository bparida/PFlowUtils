#ifndef PFOUTILS_PFODEFS_H
#define PFOUTILS_PFODEFS_H

namespace CP{

  enum PFO_JetMETConfig_inputScale { EM = 0, LC };
  enum PFO_JetMETConfig_charge { neutral = 0, charged = 1, all = 2 };

}

#endif
